<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns="http://www.infocomp.com/cbis/uk/request/1.0">
    <xsl:output method="xml" indent="yes" version="1.0"/>

    <xsl:template match="@*|*|text()">
        <xsl:copy>
            <xsl:apply-templates select="@*|*|text()" />
        </xsl:copy>
    </xsl:template>

    <!--xsl:template match="/">

        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/">

            <SOAP-ENV:Header></SOAP-ENV:Header>

                <SOAP-ENV:Body>

                    <investorAccounts>
                            <xsl:for-each select="Holdings/investorAccounts/entity">
                                <entity>
                                    <entityId><xsl:value-of select="entityId"/></entityId>
                                    <investorAccountId><xsl:value-of select="investorAccountId"/></investorAccountId>
                                    <investorname><xsl:value-of select="investorname"/></investorname>
                                    <productname><xsl:value-of select="productname"/></productname>
                                    <currentsystemdate><xsl:value-of select="currentsystemdate"/></currentsystemdate>
                                </entity>
                            </xsl:for-each>
                            <xsl:for-each select="Holdings/investorAccounts/investorAccount">
                                <investorAccount>
                                    <entityId><xsl:value-of select="entityId"/></entityId>
                                    <investorAccountId><xsl:value-of select="investorAccountId"/></investorAccountId>
                                    <investorgivennames><xsl:value-of select="investorgivennames"/></investorgivennames>
                                    <investorname><xsl:value-of select="investorname"/></investorname>
                                    <productname><xsl:value-of select="productname"/></productname>
                                    <currentsystemdate><xsl:value-of select="currentsystemdate"/></currentsystemdate>
                                </investorAccount>
                            </xsl:for-each>
                        </investorAccounts>

            </SOAP-ENV:Body>

        </SOAP-ENV:Envelope>

    </xsl:template-->

</xsl:stylesheet>
