<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:old="http://www.infocomp.com/cbis/v1/upload"
    xmlns:new="http://www.infocomp.com/conductor/cases/business/upload" 
    version="1.0">
    
    
    <xsl:param name="old-ns-prefix" select="'http://www.infocomp.com/cbis/v1'"/>
    <xsl:param name="new-ns-prefix" select="'http://www.infocomp.com/conductor/cases/business'"/>
	<xsl:param name="excluded-ns-prefix" select="'http://www.infocomp.com/cbis/v1/cbisnm'"/>
    
    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="namespace-uri(.) != $excluded-ns-prefix and starts-with(namespace-uri(.),$old-ns-prefix)">
                <xsl:element name="{local-name(.)}" namespace="{concat($new-ns-prefix,substring-after(namespace-uri(.),$old-ns-prefix))}">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="keep"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="@*|node()" mode="keep">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>
