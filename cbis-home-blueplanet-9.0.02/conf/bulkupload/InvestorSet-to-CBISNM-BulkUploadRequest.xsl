<?xml version="1.0" encoding="UTF-8"?>
<!-- 
 | Purpose: This stylesheet converts the split XML investorGroup into
 |          a CBISNM format
 | Author: Grant McDonald
 |   Date: 18/07/2006
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
 xmlns:upload="http://www.infocomp.com/cbis/v1/upload"
 version="1.0">
 <xsl:template match="/">
  <cm:CBISNM xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://www.infocomp.com/cbis/v1/cbisnm BulkUploadRequest.xsd"
   xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">
   <xsl:copy-of select="//cm:properties"/>
   <cm:payload>
    <xsl:apply-templates select="//investorSet"/>
   </cm:payload>
  </cm:CBISNM>
 </xsl:template>
 
 <xsl:template match="investorSet">
  <BulkUploadRequest xmlns="http://www.infocomp.com/cbis/v1/upload"
   xmlns:upload="http://www.infocomp.com/cbis/v1/upload">
   <investorSet>
    <xsl:copy-of select="@*"/>
    <xsl:apply-templates />
   </investorSet>
  </BulkUploadRequest>
 </xsl:template>

 <xsl:template match="entityDeclaration">
  <xsl:element name="entityDeclaration" namespace="http://www.infocomp.com/cbis/v1/upload">
    <xsl:copy-of select="@*"/>
    <xsl:copy-of select="child::*"/>
  </xsl:element>
 </xsl:template>
 
 <xsl:template match="investorAccount">
  <xsl:element name="investorAccount" namespace="http://www.infocomp.com/cbis/v1/upload">
    <xsl:copy-of select="@*"/>
    <xsl:copy-of select="child::*"/>
  </xsl:element>
 </xsl:template> 
  
 <xsl:template match="investorTransaction">
  <xsl:element name="investorTransaction" namespace="http://www.infocomp.com/cbis/v1/upload">
    <xsl:copy-of select="@*"/>
    <xsl:copy-of select="child::*"/>
  </xsl:element>
 </xsl:template>
</xsl:stylesheet>
