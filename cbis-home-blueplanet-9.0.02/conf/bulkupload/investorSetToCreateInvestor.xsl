<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  Transform an investorTransaction encapsulated in a CBISNM into a CreateCorrespondenceRequest
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:upload="http://www.infocomp.com/conductor/cases/business/upload"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
    exclude-result-prefixes="cm upload"
    version="1.0">
    
    <!-- investorId and productTypeId are determined from an ICXML
         request from the investorAccountId specified in the account
         declaration or returned from the CreateInvestorRequest
         
         serverDate and userId are attached to the above ICXML request and passed
         in for later use.
    -->
    
    <!-- 
    Uncomment this line when debugging. This will indent the transformed message
     <xsl:output method="xml" indent="yes" version="1.0"/>
     -->
    
    <!--  
    This is the main template that is used to transform the CBIS Normalised Message XML
    -->    
    <xsl:template match="/">
    	<!--TODO: Need to represent the correct service to be used within servicemix. -->
        <!-- We need to transform the payload section of the CBIS Normalised Message since this is where the paths to be remapped will be located.-->
        <xsl:apply-templates select="//upload:investorSet"/>
    </xsl:template>
    
    <!-- The investorTransaction template - this creates the CCR for us -->
    <xsl:template match="upload:investorSet">
        <xsl:variable name="adaptorGatewaySenderId" select="/cm:CBISNM/cm:properties/adaptorGatewaySenderId"/>
        <CreateInvestorRequest
         xmlns="http://www.infocomp.com/conductor/cases/business/investoraccount"
         adaptorGatewaySenderId="{$adaptorGatewaySenderId}">
            <xsl:apply-templates select="//upload:entityDeclaration"/>
            <xsl:apply-templates select="//upload:investorAccount"/>
        </CreateInvestorRequest>
        <!--<cm:CBISNM xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">-->
        	<!--<xsl:copy-of select="/cm:CBISNM/cm:properties"/>-->
        	<!--<cm:payload>-->
		        <!--<CreateInvestorRequest-->
		         <!--xmlns="http://www.infocomp.com/conductor/cases/business/investoraccount"-->
		         <!--adaptorGatewaySenderId="{$adaptorGatewaySenderId}">-->
		            <!--<xsl:apply-templates select="//upload:entityDeclaration"/>-->
		            <!--<xsl:apply-templates select="//upload:investorAccount"/>-->
		        <!--</CreateInvestorRequest>-->
        	<!--</cm:payload>-->
        <!--</cm:CBISNM>-->
    </xsl:template>
    
    <xsl:template match="upload:entityDeclaration">
        <xsl:choose>
            <xsl:when test="@userReference">
                <xsl:call-template name="processEntityDeclaration">
                    <xsl:with-param name="entity-user-ref" select="@userReference"/>    
                </xsl:call-template>                     
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="processEntityDeclaration">
                    <xsl:with-param name="entity-user-ref" select="concat('cbis-entity-',position())"/>    
                </xsl:call-template>                
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template name="processEntityDeclaration">
        <xsl:param name="entity-user-ref"/>    
        <entityDeclaration
            xmlns="http://www.infocomp.com/conductor/cases/business/investoraccount" 
            entityReferenceId="{@entityReferenceId}"
            userReference="{$entity-user-ref}">
            <xsl:copy-of select="child::*"/>
            <!--<xsl:apply-templates select="child::*"/>-->
        </entityDeclaration>        
    </xsl:template>

    <xsl:template match="upload:investorAccount">
        <xsl:choose>
            <xsl:when test="@userReference">
                <xsl:call-template name="processInvestorAccount">
                    <xsl:with-param name="account-user-ref" select="@userReference"/>    
                </xsl:call-template>                     
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="processInvestorAccount">
                    <xsl:with-param name="account-user-ref" select="concat('cbis-account-',position())"/>    
                </xsl:call-template>                
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:template>
    
    <xsl:template name="processInvestorAccount">
        <xsl:param name="account-user-ref"/>
        <investorAccount
            xmlns="http://www.infocomp.com/conductor/cases/business/investoraccount" 
            type="{@type}"
            accountOwnershipType="{@accountOwnershipType}"
            accountReferenceId="{@accountReferenceId}"
            userReference="{$account-user-ref}">
            <xsl:copy-of select="child::*"/>
        </investorAccount>
    </xsl:template>
    
</xsl:stylesheet>
