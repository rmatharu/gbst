<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  Transform an investorTransaction encapsulated in a CBISNM into a CreateCorrespondenceRequest
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common"
    xmlns:jbi="xalan://org.apache.servicemix.components.xslt.XalanExtension"
    xmlns:redirect="http://xml.apache.org/xalan/redirect"
    xmlns:cbis="http://www.infocomp.com/cbis/v1"
    xmlns:upload="http://www.infocomp.com/conductor/cases/business/upload"
    xmlns:transaction="http://www.infocomp.com/conductor/cases/business/transaction"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
    extension-element-prefixes="jbi redirect exsl"
    version="1.0">
    
    <!-- investorId and productTypeId are determined from an ICXML
         request from the investorAccountId specified in the account
         declaration or returned from the CreateInvestorRequest
         
         serverDate and userId are attached to the above ICXML request and passed
         in for later use.
    -->
    
    <!-- 
    Uncomment this line when debugging. This will indent the transformed message
    <xsl:output method="xml" indent="yes" version="1.0"/>
     -->
     
    <!--  
    This is the main template that is used to transform the CBIS Normalised Message XML
    -->    
    <xsl:template match="/">
        <!--TODO: Need to represent the correct service to be used within servicemix. -->
        <!-- We need to transform the payload section of the CBIS Normalised Message since this is where the paths to be remapped will be located.-->
        <xsl:apply-templates select="//upload:investorTransaction|//investorTransaction"/>
    </xsl:template>
    
    <!-- The investorTransaction template - this creates the CCR for us -->
    <xsl:template match="upload:investorTransaction|investorTransaction">
        <CreateCorrespondenceRequest
            xmlns="http://www.infocomp.com/conductor/cases/business/correspondence">
            <correspondenceTypeId><xsl:value-of select="/cm:CBISNM/cm:properties/correspondenceTypeId"/></correspondenceTypeId>
            <correspondenceMethod>DataFile</correspondenceMethod>
            <entityId><xsl:value-of select="/cm:CBISNM/cm:properties/investorId"/></entityId>
            <partyId><xsl:value-of select="/cm:CBISNM/cm:properties/investorAccountId"/></partyId>
            <partyType>InvestorAccount</partyType>
            <productTypeId><xsl:value-of select="/cm:CBISNM/cm:properties/productTypeId"/></productTypeId>
            <receivedDateTime>
                <xsl:choose>
                    <xsl:when test="not(transaction:correspondenceDetails/transaction:receivedDateTime)"><xsl:value-of select="concat(concat(substring(/cm:CBISNM/cm:properties/serverDate,1,10),'T'),substring(/cm:CBISNM/cm:properties/serverDate,12,8))"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="substring(transaction:correspondenceDetails/transaction:receivedDateTime,1,19)"/></xsl:otherwise>
                </xsl:choose>
            </receivedDateTime>
            <referenceAmount><xsl:value-of select="transaction:correspondenceDetails/transaction:referenceAmount"/></referenceAmount>
            <referenceNumber><xsl:value-of select="transaction:correspondenceDetails/transaction:referenceNumber"/></referenceNumber>
            <userId><xsl:value-of select="/cm:CBISNM/cm:properties/userId"/></userId>
            <notes><xsl:value-of select="transaction:correspondenceDetails/transaction:notes"/></notes>
            <xsl:copy-of select="transaction:correspondenceDetails/transaction:receivedMoney"/>
        </CreateCorrespondenceRequest>
        <!--<cm:CBISNM xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">-->
        	<!--<xsl:copy-of select="/cm:CBISNM/cm:properties"/>-->
        	<!--<cm:payload>-->
		        <!--<CreateCorrespondenceRequest-->
		        	<!--xmlns="http://www.infocomp.com/conductor/cases/business/correspondence">-->
		            <!--<correspondenceTypeId><xsl:value-of select="/cm:CBISNM/cm:properties/correspondenceTypeId"/></correspondenceTypeId>-->
		            <!--<correspondenceMethod>DataFile</correspondenceMethod>-->
		            <!--<entityId><xsl:value-of select="/cm:CBISNM/cm:properties/investorId"/></entityId>-->
		            <!--<partyId><xsl:value-of select="/cm:CBISNM/cm:properties/investorAccountId"/></partyId>-->
		            <!--<partyType>InvestorAccount</partyType>-->
		            <!--<productTypeId><xsl:value-of select="/cm:CBISNM/cm:properties/productTypeId"/></productTypeId>-->
		            <!--<receivedDateTime>-->
		                <!--<xsl:choose>-->
		                    <!--<xsl:when test="not(transaction:correspondenceDetails/transaction:receivedDateTime)"><xsl:value-of select="concat(concat(substring(/cm:CBISNM/cm:properties/serverDate,1,10),'T'),substring(/cm:CBISNM/cm:properties/serverDate,12,8))"/></xsl:when>-->
		                    <!--<xsl:otherwise><xsl:value-of select="substring(transaction:correspondenceDetails/transaction:receivedDateTime,1,19)"/></xsl:otherwise>-->
		                <!--</xsl:choose>-->
		            <!--</receivedDateTime>-->
		            <!--<referenceAmount><xsl:value-of select="transaction:correspondenceDetails/transaction:referenceAmount"/></referenceAmount>-->
		            <!--<referenceNumber><xsl:value-of select="transaction:correspondenceDetails/transaction:referenceNumber"/></referenceNumber>-->
		            <!--<userId><xsl:value-of select="/cm:CBISNM/cm:properties/userId"/></userId>-->
		            <!--<notes><xsl:value-of select="transaction:correspondenceDetails/transaction:notes"/></notes>-->
		            <!--<xsl:copy-of select="transaction:correspondenceDetails/transaction:receivedMoney"/>-->
		        <!--</CreateCorrespondenceRequest>-->
		    <!--</cm:payload>-->
		<!--</cm:CBISNM>-->
    </xsl:template>

</xsl:stylesheet>
