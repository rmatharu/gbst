<?xml version="1.0" encoding="UTF-8" ?>

<!--
  Add your own custom elements here
-->

<!-- Some sample elements -->

<!--
  Sample listener processor. It simply displays all message it receives
  together with its own "name".
-->
<!ELEMENT SampleMessageListener ANY>
<!ATTLIST SampleMessageListener
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.SampleMessageListener"
          msgTypes CDATA #IMPLIED
          id CDATA #IMPLIED
          name CDATA #IMPLIED
>

<!--
  Sample listener processor which rejects all new order messages it receives.
-->
<!ELEMENT RejectMessageListener ANY>
<!ATTLIST RejectMessageListener
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.RejectMessageListener"
          msgTypes CDATA #IMPLIED
          id CDATA #IMPLIED
>

<!--
  Sample source processor. It resends the same Fix message repeatedly
  every "interval" milliseconds.
-->
<!ELEMENT SampleMessageSource ANY>
<!ATTLIST SampleMessageSource
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.SampleMessageSource"
          id CDATA #IMPLIED
          checkBeforeSend ( true | false ) "false"
          interval CDATA "10000"
          targets CDATA #IMPLIED
          senders CDATA #IMPLIED
>

<!--
  Sample source processor. It resends Fix messages read from files picked randomly
  every "interval" milliseconds.
-->
<!ELEMENT RandomFileSource ANY>
<!ATTLIST RandomFileSource
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.RandomFileSource"
          id CDATA #IMPLIED
          checkBeforeSend ( true | false ) "false"
          interval CDATA "2000"
          inputDir CDATA #REQUIRED
          tagSeperator CDATA #IMPLIED
          readLineByLine ( true | false ) "true"
          transformer CDATA "com.cameronsystems.fix.message.RawTransformer"
>

<!--
  Sample source/listener processor that ack incoming orders with an exec
  report.
-->
<!ELEMENT SampleExecutor ANY>
<!ATTLIST SampleExecutor
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.SampleExecutor"
          id CDATA #IMPLIED
          interval CDATA "1000"
>

<!--
  Sample validating listener processor.
-->
<!ELEMENT SampleValidatingListener ANY>
<!ATTLIST SampleValidatingListener
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.SampleValidator"
          msgTypes CDATA #IMPLIED
          id CDATA #IMPLIED
          minQuantity CDATA #IMPLIED
>

<!--
  Sample custom monitor.
-->
<!ELEMENT SampleMonitor ANY>
<!ATTLIST SampleMonitor
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.SampleMonitor"
          id CDATA #IMPLIED
>

<!--
  Sample input message persister.
-->
<!ELEMENT SampleInputPersister ANY>
<!ATTLIST SampleInputPersister
          class CDATA #FIXED "com.cameronsystems.fix.util.persistence.SampleInputMessagePersister"
          id CDATA #IMPLIED
>

<!--
  Sample Universal Server message processor which processes incoming FIX
  market data messages and updates an internal market data service which
  can be viewed by a browser.

  depthlimit
      Optional limit on depth requested in subscriptions.

      If not specified, full depth is requested.

  entryTypesDepth
      Optional space delimited list of MDEntryType's requested for depth data.

      If not specified, defaults to common values (bid, offer) - ie "0 1".

  entryTypesTob
      Optional space delimited list of MDEntryType's requested for top of
      book data.

      If not specified, defaults to common values (bid, offer, trade etc)
      ie "0 1 2...".

  instruments
      Specifies which instruments will be subscribed for.

      One or more instrument symbols may be specified, separated by spaces.

      If neither this attribute nor the instrumentsFile attribute is specified,
      it will automatically subscribe to
      the 26 symbols "A" to "Z" - ie the letters of the alphabet.

  instrumentsFile
      Specifies file containing the instruments which will be subscribed for.

      The format of the file is as follows:

      First two lines are ignored (comments).

      Then one symbol per line (which may be in quotes (") if you wish) followed
      by an optional comma (,) and some comment text.

      For example:

      SYMBOL1
      SYMBOL2
      :

      or

      SYMBOL1,some text
      SYMBOL2,some text
      :

      If neither this attribute nor the instruments attribute is specified,
      it will automatically subscribe to
      the 26 symbols "A" to "Z" - ie the letters of the alphabet.

  serviceid
      Optional name of service to be populated by this processor.

      If not specified a default service is automatically created.

      If specified, this should match the id of a market data service
      declared in the "Services" section of the configuration file.

  subscribe
      Determines whether or not the message processor subscribes for depth
      aggregated by price.
      Allowed values are:

      "depth" - Aggregated by price
      "detail" - Unaggregated

  update
      Determines whether full or incremental updates are requested
      (corresponding to FIX MDUpdateType field - tag 265)

      Allowed values are:

      "full"
      "incremental"

      The type of update requested must match what the underlying market is
      capable of.
-->
<!ELEMENT SampleMarketDataClientProcessor ANY>
<!ATTLIST SampleMarketDataClientProcessor
          class CDATA #FIXED "com.cameronsystems.fix.marketdata.SampleMarketDataClientProcessor"
          id CDATA #IMPLIED
          refid CDATA #IMPLIED

          depthlimit      CDATA #IMPLIED
          entryTypesTob   CDATA #IMPLIED
          entryTypesDepth CDATA #IMPLIED
          instruments     CDATA #IMPLIED
          instrumentsFile CDATA #IMPLIED
          serviceid       CDATA #IMPLIED
          subscribe (depth|detail) "detail"
          update (full|incremental) #IMPLIED
>

<!ELEMENT SampleEntitlementsService ANY>
<!ATTLIST SampleEntitlementsService
          class CDATA #FIXED "com.cameronsystems.fix.marketdata.SampleEntitlementsService"
          id CDATA #IMPLIED
>


<!--
  Sample listener processor. It displays the rate at which messages come into the system.
-->
<!ELEMENT PerformanceMessageTester ANY>
<!ATTLIST PerformanceMessageTester
          class CDATA #FIXED "com.cameronsystems.fix.universalserver.test.PerformanceMessageTester"
          id CDATA #IMPLIED
          cycles CDATA #IMPLIED
          msgTypes CDATA #IMPLIED
>

<!--
   Encryption Example
-->
<!ELEMENT EncryptionSource ANY>
<!ATTLIST EncryptionSource
          class CDATA #FIXED "com.cameronsystems.fix.processor.EncryptionSource"
          id CDATA #IMPLIED
          encrypt.fields CDATA #IMPLIED
          encrypt.all.possible.fields (true | false) "false"
          encrypt.recommended.fields (true | false) "true"
>

<!--
  MessageEmailLogger
-->
<!ELEMENT MessageEmailLogger ANY>
<!ATTLIST MessageEmailLogger
          class CDATA #FIXED "com.cameronsystems.fix.processor.MessageEmailLogger"
          id CDATA #IMPLIED
          loggerName CDATA #IMPLIED
          msgType CDATA #REQUIRED
>

<!--
  OMS Simulator.
-->
<!ELEMENT BuySideOMS ANY>
<!ATTLIST BuySideOMS
          class CDATA #FIXED "com.cameronsystems.fix.oms.simulator.processor.SimulatorBuyMessageProcessor"
          id CDATA "Buy Side"
          file.name CDATA "buy_sim.oms"
          is.ha (true | false) "false"
>

<!ELEMENT SellSideOMS ANY>
<!ATTLIST SellSideOMS
          class CDATA #FIXED "com.cameronsystems.fix.oms.simulator.processor.SimulatorSellMessageProcessor"
          id CDATA "Sell Side"
          file.name CDATA "sell_sim.oms"
          is.ha (true | false) "false"
>
<!ELEMENT RoutingExampleSource ANY>
<!ATTLIST RoutingExampleSource
          class CDATA #FIXED "com.cameronsystems.fix.processor.routing.RoutingExampleSource"
          id CDATA #IMPLIED
          interval CDATA "5000"
          side (buy | sell) "buy"
          deliverTo CDATA #IMPLIED
          onBehalfOf CDATA #IMPLIED
>

<!ELEMENT MultiHopRoutingRules ANY>
<!ATTLIST MultiHopRoutingRules
          class CDATA #FIXED "com.cameronsystems.fix.processor.routing.MultiHopRoutingRules"
          id CDATA #IMPLIED
          hideRoutingFields (true | false) "false"
>
