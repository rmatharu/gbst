<?xml version="1.0" encoding="UTF-8"?>
<!--
    ==============================================================================================================
    How to reference CBIS properties, system properties and environment variables within reporting properties file
    ==============================================================================================================
    You can use the following special variables to reference appropriate values.

    * $cbis-prop:<cbis-property-name>
      References a CBIS property called <cbis-property-name>.
      You can reference any CBIS properties defined within CBIS_HOME/cbis.properties file.
      The property value may also refer to other CBIS properties, system properties or environment variables (i.e. cbis.work.dir=${CBIS_HOME}/work).

    * $cbis-system-prop:<system-property-name>
      References a system property called <system-property-name>.
      You can reference any system properties set using -D<system-property-name>=<system-property-name> option within the command used to start CBIS.

    * $cbis-env-var:<environment-variable-name>
      References an environment variable called <environment-variable-name>. Those environment variables must be set prior to staring CBIS.

    ==============================================================================================================
    How to evaluate the xpath set within a CBIS property, system property or environment variable
    ==============================================================================================================
    If a CBIS property, system property or environment variable contains xpath, a special function cbis:eval-xpath(node, xpath) must to be used,
    where node will usually be the root element (. or /), and xpath will be the property that contains the xpath.

    If you have configured the xpath middle (i.e. cbis.reporting.filename.xpathmiddle within CBIS_HOME/cbis.properties) to use any of the properties identified within the AfterExtract[ReportName] (i.e. AfterExtractReportBenefitLetterAdvices) property set,
    you must use cbis:eval-xpath() function to resolve the correct file name.

    Here are examples of configuring the body of the email that gets sent for the email delivery option to the HTML file that gets generated.
    <property name="cbis-email-body-filename">
        <xpath-expression>concat($cbis-prop:cbis.reporting.dir.reports.approved, '/', //cm:properties/cbis-filenamestub-prefix, cbis:eval-xpath(/, $cbis-prop:cbis.reporting.filename.xpathmiddle), //cm:properties/cbis-filenamestub-suffix, '.html')</xpath-expression>
    </property>

    If you want to use the existing cbis-filenamestub reporting property, the following 2 reporting properties must be configured.
    <property name="cbis-filenamestub">
        <xpath-expression>concat(//cm:properties/cbis-filenamestub-prefix,cbis:eval-xpath(/, $cbis-prop:cbis.reporting.filename.xpathmiddle), //cm:properties/cbis-filenamestub-suffix)</xpath-expression>
    </property>
    <property name="cbis-email-body-filename">
        <xpath-expression>concat($cbis-prop:cbis.reporting.dir.reports.approved, '/', //cm:properties/cbis-filenamestub, '.html')</xpath-expression>
    </property>

    Please note that to correctly reference reporting properties within an xpath, those reporting properties must be already resolved. Reporting properties are resolved in the appearing order
    within reporting properties files.

    For example, reporting-property-2 reporting property will have the value "reporting-property-1-value-reporting-property-2-value-",
    as the value of reporting-property-3 has not been set yet.
    <property name="reporting-property-1">
        <static-value>reporting-property-1-value</static-value>
    </property>
    <property name="reporting-property-2">
        <xpath-expression>concat(//cm:properties/reporting-property-1, '-', 'reporting-property-2-value', '-', //cm:properties/reporting-property-3)</xpath-expression>
    </property>
    <property name="reporting-property-3">
        <static-value>reporting-property-3-value</static-value>
    </property>
-->
<mps xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">
    <!-- =========================================== -->
    <!-- Report - Benefit Letter Advices      -->
    <!-- =========================================== -->
    <!--
        Properties Used for the report Extract and Report Generation.
    -->
<property-set name="BeforeExtractReportBenefitLetterAdvices">
        <!--
            This indicates whether or not QA step is required.

            * The valid values are true and false (case insensitive).
            * If this property is not set or no value is set, the default value of true will be applied.
            * If this is set to false, QA step will be skipped and the reports will immediately go into the delivery stage once report generation succeeds.
        -->
        <property name="cbis-report-approval-required">
            <existing-property/>
            <static-value>false</static-value>
        </property>

        <!--
            This property sets the template file for your report. You can specify either an Windward template or an XSL file.

            * When an Windward template is specified, CBIS will generate a PDF and WordML report for each row (i.e. investor account ) returned by the ICXML component.
            * If cbis-report-format is also set, you can only generate one format or report file.

            * When an XSL file is specified, you must set cbis-report-template. You can only generate 1 format of report file (unless cbis-report-template-<id> and
              cbis-report-format-<id> is set).
        -->
        <property name="cbis-report-template">
            <existing-property/>			
			<static-value>conf/reports/BenefitLetter/BenefitLetter_Template.docx</static-value>            
        </property>
        <!--
            * When the template specified by cbis-report-template is a Windward template, you can generate a specific format of report instead of both
              PDF and WordML reports. The valid values and the resulting report file extensions are;

              * pdf -> .pdf
              * xls -> .xls
              * xlsx -> .xlsx
              * excelml -> .xls
              * docx -> .docx
              * wordml -> .doc
              * html -> .html
              * htm -> .htm
              * text -> .txt
              * txt -> .txt
              * rtf -> .rtf
              * pptf -> .pptx

            * The values are case insensitive.

            * When the template specified by cbis-report-template is an XSL file, you must set cbis-report-format. The value you set in cbis-report-format will be
              the file extension (i.e csv -> .csv).
            * The only exception is that you cannot use "xml" in cbis-report-format. Please use the cbis-report-template-<id> and cbis-report-format-<id> pair instead
              if you want to produce an xml output using an XSL template.
        -->
        <property name="cbis-report-format">
            <existing-property/>
            <static-value>PDF</static-value>
        </property>

        <!--
            cbis-report-template-<id> and cbis-report-format-<id> pair

            * You can generate more than 1 format of report files by nominating pair(s) of cbis-report-template-<id> and cbis-report-format-<id> properties.
            * When this property is set, you need to also provide the matching cbis-report-format-<id> property.
            * <id> can be any alpha numeric characters, such cbis-report-template-1 or cbis-report-template-xlsx.
            * cbis-report-template-<id> can also be used in conjunction with cbis-report-template (without ID).
            * This generated file will have the <id> in the file name (i.e. xxx.<id>.<extension>).
        -->
        <property name="cbis-report-template-xsl">
            <existing-property/>
            <static-value>conf/reports/CopyListResults.xsl</static-value>
        </property>
        <!--
            cbis-report-template-<id> and cbis-report-format-<id> pair

            * You can generate more than 1 format of report files by nominating pair(s) of cbis-report-template-<id> and cbis-report-format-<id> properties.
            * When this property is set, you need to also provide the matching cbis-report-template-<id> property.
            * <id> can be any alpha numeric characters, such cbis-report-format-1 or cbis-report-format-xlsx.
            * cbis-report-format-<id> can also be used in conjunction with cbis-report-format (without ID).
            * This generated file will have the <id> in the file name (i.e. xxx.<id>.<extension>).
        -->
        <property name="cbis-report-format-xsl">
            <existing-property/>
            <static-value>xml</static-value>
        </property>

        <!-- If specified, CBIS will generate a HTML file used for the body of the email sent for the email delivery option. -->
        <property name="cbis-email-template">
            <existing-property/>
           <!--static-value>conf/reports/BenefitLetter/BenefitLetter_email_body_Template.docx</static-value-->
        </property>
        <!-- After each report is generated, you can send a JMS message. -->
        <property name="cbis-report-approval-jms">
            <existing-property/>
            <static-value>false</static-value>
        </property>
        <!-- After the admin Email is sent  do you want to send a JMS request. -->
        <property name="cbis-report-run-approval-jms">
            <existing-property/>
            <static-value>false</static-value>
        </property>
    </property-set>

    <!--
        The following property set is used for the Report Delivery Investor Email properties.
    -->
    <property-set name="AfterExtractReportBenefitLetterAdvices">
        <property name="cbis-email-from">
            <existing-property/>
            <static-value></static-value>
        </property>
        <property name="cbis-email-to">
            <existing-property/>
             <static-value></static-value>            
        </property>
        <property name="cbis-email-subject">
            <existing-property/>
            <static-value>Benefit Letter</static-value>
        </property>

        <!--
            * By providing this property, you can optionally rename the file attachment file sent with the email delivery.
            * The ID in the property name cbis-email-attachment-display-filename<id> needs to match with the ID used in cbis-email-attachment-filename<id>.
        -->
        <property name="cbis-email-attachment-display-filename-generated">
            <existing-property/>
        </property>
        <!--
            This specifies the party ID of this report.

            * When create correspondence option is on, this value will be used to populate correspondence.party_id column.
        -->
        <property name="cbis-party-id">
            <existing-property/>
               <xpath-expression>//*[local-name()='BenefitLetterAdvices']/*[local-name()='benefitLetterRecipients']/*[local-name()='benefitLetterRecipient']/*[local-name()='memberAccountId']</xpath-expression>
        </property>
        <!--
            This specifies the party type ID of this report.

            * When create correspondence option is on, this value will be used to populate correspondence.party_type_id column.
        -->
        <property name="cbis-party-type-id">
            <existing-property/>            
            <static-value>1</static-value>
        </property>
        <!--
            This specifies the fund ID related to the report.

            * This value will be set on cbis_msg.fund_id.
            * This value will be validated to ensure that this is a valid fund ID, and is the correct fund for
              the subfund if specified by cbis-subfund-id, the product type if specified by cbis-product-type-id, and the portfolio if specified by cbis-portfolio-id.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <!--property name="cbis-fund-id">
            <existing-property/>
            <xpath-expression>//results/fundId</xpath-expression>
        </property-->
        <!--
            This specifies the subfund ID related to the report.

            * This value will be set on cbis_msg.subfund_id.
            * This value will be validated to ensure that this is a valid subfund ID, and is the correct subfund for
              the product type if specified by cbis-product-type-id, and the portfolio if specified by cbis-portfolio-id.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <!--property name="cbis-subfund-id">
            <existing-property/>
            <xpath-expression>//results/subfundId</xpath-expression>
        </property-->
        <!--
            This specifies the product type ID related to the report.

            * This value will be set on cbis_msg.product_type_id.
            * When create correspondence option is on, this value will be used to populate correspondence.product_type_id column.
            * This value will be validated to ensure that this is a valid product type ID, and is the correct product type for the portfolio if specified by cbis-portfolio-id.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <!--property name="cbis-product-type-id">
            <existing-property/>
            <xpath-expression>//results/productTypeId</xpath-expression>
        </property-->
        <!--
            This specifies the portfolio ID related to the report.

            * This value will be set on cbis_msg.portfolio_id.
            * This value will be validated to ensure that this is a valid portfolio ID.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <!--property name="cbis-portfolio-id">
            <existing-property/>
            <xpath-expression>//results/portfolioId</xpath-expression>
        </property-->
        <!--
            This specifies the notes related to the report.

            * This value will be set on cbis_msg.notes.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <!--property name="cbis-msg-notes">
            <existing-property/>
            <xpath-expression>//results/notes</xpath-expression>
        </property-->
        <!--
            This specifies the owner of the bulk report run.

            * This value will be set on cbis_run.user_reference.
            * This value will be visible within CBIS Console, and in the CBIS Message Extract shared process within Composer.
        -->
        <property name="cbis-report-owner">
            <existing-property/>
            <xpath-expression><![CDATA[//cbisReportOwner]]></xpath-expression>
        </property>
        <property name="cbis-ftp-subdirectory">
            <existing-property/>
            <xpath-expression>concat(//cm:properties/cbis-processname,'/',translate(substring(//cbis-timestamp,1,10),'-',''))</xpath-expression>
        </property>
        <!-- Used when cbis-report-delivery-collate = true, usually the client will have a separate windward template.-->
        <property name="cbis-collate-template">
            <existing-property/>
            <!--static-value>conf/reports/BenefitLetter/BenefitLetter_email_body_Template.docx</static-value-->
        </property>
        <property name="cbis-report-delivery-email">
            <existing-property/>
            <static-value>false</static-value>
			<!--xpath-expression>//MemberAccounts/recipient/deliveryViaEmail</xpath-expression-->
        </property>
        <property name="cbis-report-delivery-ftp">
            <existing-property/>
            <static-value>false</static-value>
        </property>
        <property name="cbis-report-delivery-ftp-server">
            <existing-property/>
            <static-value>false</static-value>
        </property>
        <property name="cbis-report-delivery-imanage">
            <existing-property/>
            <static-value>false</static-value>
        </property>
        <property name="cbis-report-delivery-jms">
            <existing-property/>
            <static-value>false</static-value>
        </property>
        <!--
            This property is mainly meant to be used for a third-party mailhouse product which handles the actual report delivery.

            * This value will be set on cbis_msg.delivery_method.
            * This value will also be visible in the CBIS Message Extract shared process within Composer.
        -->
        <property name="cbis-report-delivery-mailhouse">
            <existing-property/>
            <!--xpath-expression>//BenefitLetterAdvices/benefitLetterRecipients/benefitLetterRecipient/deliveryMethod</xpath-expression-->
        </property>
        <property name="cbis-report-delivery-collate">
            <existing-property/>
            <static-value>false</static-value>
        </property>

        <!--
            This indicates whether or not reports will be delivered to a nominated network location.

            * The valid values are true and false.
            * If this property is not set or contains no value, the default value of false will be applied.
            * If this property is set to true, cbis-report-delivery-file-system must be set with a valid network location.
        -->
        <property name="cbis-report-delivery-file-system">
            <existing-property/>
            <static-value>true</static-value>
        </property>
        <!--
            This nominates a network location for file system delivery. This must be a full path, and it must be accessible by CBIS.

            * This property is only relevant when cbis-report-delivery-file-system is set to true, and must be set to a valid network location.
            * If the directory doesn't exist, CBIS will attempt to create the directory (and any nesting directories if required).
        -->
        <property name="cbis-report-delivery-file-system-location">
            <existing-property/>
            <xpath-expression>concat($cbis-prop:cbis.adhoc.reporting.base.dir,'-reporting/file-system-delivery-',//cm:properties/cbis-processname,'/',translate(substring(//cbis-timestamp,1,10),'-',''))</xpath-expression>
        </property>

        <!--
            This indicates whether or not to create a correspondence for each report. Report files will be attached to the correspondence as correspondence attachments.

            * The valid values are true and false.
            * If this property is not set or contains no value, the default value of false will be applied.
            * If this property is set to true, cbis-report-correspondence-type must be set with a valid correspondence type ID.
        -->
        <property name="cbis-report-create-correspondence">
            <existing-property/>           
		   <xpath-expression>//*[local-name()='BenefitLetterAdvices']/*[local-name()='benefitLetterRecipients']/*[local-name()='benefitLetterRecipient']/*[local-name()='createOutgoingCorrespondence']</xpath-expression>
        </property>
        <!--
            This indicates correspondence type ID for the correspondence generated when cbis-report-create-correspondence is set to true.

            * This property is only relevant is cbis-report-create-correspondence is set to true.
            * The value must be a valid correspondence type configured in Composer.
        -->
        <property name="cbis-report-correspondence-type">
            <existing-property/>
           <static-value>533</static-value>
        </property>
		
		<property name="cbis-outgoing-correspondence-id">
            <xpath-expression>concat(substring('000000000', 1, (9 - string-length(//cbis-msg-id))), //cbis-msg-id)</xpath-expression>
        </property>
		
        <!--
            This indicates an optional sub directory where correspondence attachments will be saved to when cbis-report-create-correspondence is set to true.

            * The base directory is specified by cbis.reporting.corro.attachment.basedir property in CBIS_HOME/cbis.properties file.
            * This property is only relevant if cbis-report-create-correspondence is set to true.
            * If the directory doesn't exist, CBIS will attempt to create the directory (and any sub directories if required).
        -->
        <property name="cbis-report-correspondence-attachment-subdirectory">
            <existing-property/>
            <xpath-expression>concat(//cm:properties/cbis-processname,'/',translate(substring(//cbis-timestamp,1,10),'-',''))</xpath-expression>
        </property>
        <!--
            This specifies the name of any report insert(s) that are added to the report outside of CBIS (e.g. mailhouse).
            If this value is set, the value will be set on the correspondence notes.
        -->
        <property name="cbis-report-insert-name">
            <existing-property/>
        </property>

        <property name="cbis-email-body-filename">
            <existing-property/>
            <xpath-expression>concat($cbis-prop:cbis.reporting.dir.reports.approved, '/', //cm:properties/cbis-filenamestub-prefix, cbis:eval-xpath(/, $cbis-prop:cbis.reporting.filename.xpathmiddle), //cm:properties/cbis-filenamestub-suffix, '.html')</xpath-expression>
        </property>
        <!--
            * cbis-email-attachment-filename* specifies the files to be attached to the email sent with the email delivery.
            * You can nominate more than 1 attachment file by using ID in the property name cbis-email-attachment-filename<id> (i.e. cbis-email-attachment-filename-generated).
        -->
        <property name="cbis-email-attachment-filename-generated">
            <xpath-expression>concat($cbis-prop:cbis.reporting.dir.reports.approved, '/', //cm:properties/cbis-filenamestub-prefix, cbis:eval-xpath(/, $cbis-prop:cbis.reporting.filename.xpathmiddle), //cm:properties/cbis-filenamestub-suffix, '.pdf')</xpath-expression>
        </property>
    </property-set>

</mps>


