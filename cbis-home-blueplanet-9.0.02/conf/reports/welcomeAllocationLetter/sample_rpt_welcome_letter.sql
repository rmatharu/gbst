IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'sample_rpt_welcome_letter')
BEGIN
	PRINT 'Dropping Procedure sample_rpt_welcome_letter'
	DROP  PROCEDURE  sample_rpt_welcome_letter
END
GO

PRINT 'Creating Procedure sample_rpt_welcome_letter'
GO

CREATE PROCEDURE sample_rpt_welcome_letter (
	/* Param List */
	@ProductID int,
	@row_limit int = 0
)		
AS
	declare	@vc_errorMsg	varchar(255), 
			@i_errorCode	integer

	if (@row_limit is null) 
		set rowcount 0
	else
		set rowcount  @row_limit

	/*
	 * DEF04317
	 * Uses a temp table to ensure that stored procedures using temporary tables will not cause a transactional error in bulk reporting.
	 */
	create table #investor_details (
		investor_number		integer not null,
		salutation			varchar(50) null,
		name				varchar(60) not null,
		investor_name		varchar(121) not null,
		balance_units		decimal(18,6) null,
		net_asset_value		decimal(18,6) null,
		street_number		varchar(12) null,
		street				varchar(40) null,
		suburb				varchar(40) null,
		postcode			varchar(8) null,
		statecode			varchar(10) null,
		portfolio_name		varchar(40) not null,
		product_name		varchar(40) not null
	)

	select @i_errorCode = @@error
	if (@i_errorCode != 0)
	begin
		select @vc_errorMsg = 'An error has occurred while creating #investor_details.'
		RAISERROR 99999 @vc_errorMsg
		return -100
	end
	
	if not exists (select 1 from product_type where product_type_id =  @ProductID)
	begin
		select @vc_errorMsg ="The product id "+convert(varchar,@ProductID)+" is invalid."
		RAISERROR 99999 @vc_errorMsg
		return -100
	end

	insert into #investor_details (
		investor_number,
		salutation,
		name,
		investor_name,
		balance_units,
		net_asset_value,
		street_number,
		street,
		suburb,
		postcode,
		statecode,
		portfolio_name,
		product_name
	)
	SELECT	investor_number = investor_entity.entity_id,
			salutation = investor_entity.salutation,
			name = investor_entity.name,
			investor_name = investor_entity.given_names + ' ' +  investor_entity.name,
			balance_units = mai.balance_units,
			net_asset_value = convert(decimal(18, 6),round(mai.balance_units * pup.sell_unit_price,6)),
			street_number = addr.street_number,
			street = addr.street,
			suburb = addr.suburb,
			postcode = addr.postcode,
			statecode = addr.statecode,
			portfolio_name = port.name,
			product_name = prod.name
	FROM	member_account_investment mai,
			member_account macc,
			entity investor_entity,
			address addr,
			portfolio_unit_price pup,
			portfolio port,
			product_type prod
	WHERE	macc.member_account_id = mai.member_account_id
	AND		investor_entity.entity_id = macc.entity_id
	AND		addr.entity_id =* investor_entity.entity_id
	AND		pup.portfolio_id = mai.portfolio_id
	AND		mai.portfolio_id = port.portfolio_id
	AND		macc.product_type_id = @ProductID
	AND		prod.product_type_id = macc.product_type_id
	AND		pup.as_at_date = 
			(SELECT max(as_at_date)
			FROM portfolio_unit_price
			WHERE as_at_date <= getdate()
			AND portfolio_id = mai.portfolio_id)

	select @i_errorCode = @@error
	if (@i_errorCode != 0)
	begin
		select @vc_errorMsg = 'An error has occurred while inserting data into #investor_details.'
		RAISERROR 99999 @vc_errorMsg
		return -100
	end
	
	select	investor_number,
			salutation,
			name,
			investor_name,
			balance_units,
			net_asset_value,
			street_number,
			street,
			suburb,
			postcode,
			statecode,
			portfolio_name,
			product_name
	from	#investor_details
	
	select @i_errorCode = @@error
	if (@i_errorCode != 0)
	begin
		select @vc_errorMsg = 'An error has occurred returning result set.'
		RAISERROR 99999 @vc_errorMsg
		return -100
	end
	
	drop table #investor_details
	
	set rowcount 0
GO

if exists (select 1
             from sysusers
            where uid = gid
              and upper(name) = 'READONLYGROUP')
   grant execute on sample_rpt_welcome_letter to ReadOnlyGroup
GO

if exists (select 1
             from sysusers
            where uid = gid
              and upper(name) = 'READWRITEGROUP')
   grant execute on sample_rpt_welcome_letter to ReadWriteGroup
GO

if exists (select 1
             from sysusers
            where uid = gid
              and upper(name) = 'PUBLIC')
   grant execute on sample_rpt_welcome_letter to public
GO
