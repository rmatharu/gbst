<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" 
       xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">
    <xsl:template match="cm:CBISNM">
        <xsl:copy-of select="cm:payload/child::*[1]"/>
    </xsl:template>
</xsl:stylesheet>