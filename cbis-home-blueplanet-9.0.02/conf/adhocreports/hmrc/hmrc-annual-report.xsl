<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="http://www.govtalk.gov.uk/taxation/PSOnline/SchemeEventReport/10">   <!-- temporary change from 2 to 8 -->
    <xsl:output method="xml" indent="yes"/>

    <!--
        | if payment_type is of value of 'benefit-in-kind', 'employer','non-recognised', 'error',
    	| 'non-relevant-factor','benefits','loans','residential-property', 'tangible-property' use Nature,
    	| otherwise use Other
    	-->
    <xsl:variable name="NATURE_PATTERN"
                  select="'benefit-in-kind/employer/non-recognised/error/non-relevant-factor/benefits/loans/residential-property/tangible-property'"/>

    <!--Added as part of  Tracker Issue 3312 to fix the Organization name problem - Starts Here-->
    <xsl:variable name="RECIPIENT_TYPE" select="'I'"/>
    <!--Added as part of  Tracker Issue 3312 to fix the Organization name problem - Ends Here-->
    <xsl:variable name="EXTERNAL_FLAG" select="'Y'"/>

    <!--Added as part of  Tracker Issue 3508 to fix the UnauthorisedPayments Tag Repetition problem - Starts Here-->
    <xsl:variable name="Member_Details"
                  select="MemberDetails[event_type='UnauthorisedPayments']"/>
    <xsl:variable name="Employer_Details"
                  select="EmployerDetails[event_type='UnauthorisedPayments']"/>
    <!--Added as part of  Tracker Issue 3508 to fix the UnauthorisedPayments Tag Repetition problem - Ends Here-->

    <xsl:template match="hmrc">
        <SchemeEventReport>

            <!-- hard code version number -->
            <VersionNumber>0</VersionNumber>

            <!-- process compulsory AmendedReturn element and create new tag Amendment-->
            <Amendment>
                <xsl:value-of select="SchemeDetails/AmendedReturn"/>
            </Amendment>

            <xsl:apply-templates select="SchemeDetails"/>
            <xsl:apply-templates select="BandDetails"/>

            <xsl:call-template name="writeUnAuthorizedPayments"/>
            <xsl:call-template name="writePaymentsExceedingHalfSLA"/>
            <xsl:call-template name="writeBenefitCrystallisation"/>
            <xsl:call-template name="writeLargeCommencementLumpSums"/>
            <xsl:call-template name="writeEnhancedProtectionLumpSums"/>
            <xsl:call-template name="writeFlexibleDrawdownPayments"/>
            <xsl:call-template name="writeAnnualAllowance" />
            <xsl:apply-templates select="Declaration"/>

        </SchemeEventReport>
    </xsl:template>

    <!--
        | process SchemeDetails: this is a compulsory element with two compulsory elements and one optional element
        | which are among one choosen group, if all of them are empty, just give <TaxReference/>
        -->
    <xsl:template match="SchemeDetails">
        <SchemeDetails>
            <xsl:copy-of select="SchemeName"/>
            <xsl:copy-of select="YearEnded"/>
            <xsl:choose>
                <xsl:when test="boolean(TaxReference[string-length(text())>0])">
                    <xsl:copy-of select="TaxReference"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- in case of non of above elements shows up, just give a empty TaxReference,
                       cause this is compulsory element -->
                    <TaxReference> </TaxReference>
                </xsl:otherwise>
            </xsl:choose>
        </SchemeDetails>
    </xsl:template>

    <!--
        | process BandDetails: this is a optional element;
        | when change_in_band equals Yes, create NumberOfMembers node.
        -->
    <xsl:template match="BandDetails">
        <!-- <xsl:if test="boolean(change_in_band[text()='Yes'])">  -->
        <xsl:if test="boolean(new_band[string-length(text())>0])">
            <NumberOfMembers>
                <NumberBand>
                    <xsl:value-of select="new_band"/>
                </NumberBand>
            </NumberOfMembers>
        </xsl:if>
    </xsl:template>

    <!--
        | process UnAuthorizedPayments: this is a optional element;
    -->
    <!--Added as part of  Tracker Issue 3508 to fix the UnauthorisedPayments Tag Repetition problem - Starts Here-->
    <xsl:template name="writeUnAuthorizedPayments">
        <xsl:if  test="($Member_Details or $Employer_Details)">
            <UnauthorisedPayments>
                <xsl:if  test="($Member_Details )">
                    <xsl:call-template name="writeMemberDetails">
                        <xsl:with-param name="eventType" select="'UnauthorisedPayments'"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if  test="($Employer_Details)">
                    <xsl:apply-templates select="EmployerDetails"/>
                </xsl:if>
            </UnauthorisedPayments>
        </xsl:if>
    </xsl:template>
    <!--Added as part of  Tracker Issue 3508 to fix the UnauthorisedPayments Tag Repetition problem - Ends Here-->

    <!--
        | process PaymentsExceedingHalfSLA: this is a optional element;
        -->
    <xsl:template name="writePaymentsExceedingHalfSLA">
        <xsl:if
                test="boolean(MemberDetails[event_type='PaymentsExceedingHalfSLA'])">
            <PaymentsExceedingHalfSLA>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'PaymentsExceedingHalfSLA'"/>
                </xsl:call-template>
            </PaymentsExceedingHalfSLA>
        </xsl:if>
    </xsl:template>

    <!--
        | process BenefitCrystallisation: this is a optional element;
        -->
    <xsl:template name="writeBenefitCrystallisation">
        <xsl:if
                test="boolean(MemberDetails[event_type='BenefitCrystallisation'])">
            <BenefitCrystallisation>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'BenefitCrystallisation'"/>
                </xsl:call-template>
            </BenefitCrystallisation>
        </xsl:if>
    </xsl:template>

    <!--
        | process LargeCommencementLumpSums: this is a optional element;
        -->
    <xsl:template name="writeLargeCommencementLumpSums">
        <xsl:if
                test="boolean(MemberDetails[event_type='LargeCommencementLumpSums'])">
            <LargeCommencementLumpSums>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'LargeCommencementLumpSums'"/>
                </xsl:call-template>
            </LargeCommencementLumpSums>
        </xsl:if>
    </xsl:template>

    <!--
        | process EnhancedProtectionLumpSums: this is a optional element;
        -->
    <xsl:template name="writeEnhancedProtectionLumpSums">
        <xsl:if
                test="boolean(MemberDetails[event_type='EnhancedProtectionLumpSums'])">
            <EnhancedProtectionLumpSums>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'EnhancedProtectionLumpSums'"/>
                </xsl:call-template>
            </EnhancedProtectionLumpSums>
        </xsl:if>
    </xsl:template>

    <xsl:template name="writeAnnualAllowance">
        <xsl:if
                test="boolean(MemberDetails[event_type='AnnualAllowance'])">
            <AnnualAllowance>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'AnnualAllowance'"/>
                </xsl:call-template>
            </AnnualAllowance>
        </xsl:if>
    </xsl:template>

    <!--
    | process FlexibleDrawdownPayments: this is a optional element;
    -->
    <xsl:template name="writeFlexibleDrawdownPayments">
        <xsl:if
                test="boolean(MemberDetails[event_type='FlexibleDrawdownPayments'])">
            <FlexibleDrawdownPayments>
                <xsl:call-template name="writeMemberDetails">
                    <xsl:with-param name="eventType" select="'FlexibleDrawdownPayments'"/>
                </xsl:call-template>
            </FlexibleDrawdownPayments>
        </xsl:if>
    </xsl:template>

    <!--
        | process MemberDetails: called by above tempates with event_type as parameter
        -->
    <xsl:template name="writeMemberDetails">
        <xsl:param name="eventType"/>
        <xsl:apply-templates select="MemberDetails">
            <xsl:with-param name="eventTypePassed" select="$eventType"/>
        </xsl:apply-templates>
    </xsl:template>


    <!--
        | process MemberDetails tag : called by writeMemberDetails template
        | according to event_type value pssed as paramater, process different types of member details
        -->
    <xsl:template match="MemberDetails">
        <xsl:param name="eventTypePassed"/>
        <xsl:choose>
            <xsl:when test="(event_type='UnauthorisedPayments') and (event_type=$eventTypePassed)">
                <Recipient>
                    <xsl:choose>
                        <xsl:when test="boolean(ext_entity_flag[contains($EXTERNAL_FLAG, text())])">
                            <OnBehalfMember>
                                <xsl:if test="boolean(pstr[string-length(text())>0])">
                                    <Pstr><xsl:value-of select="pstr"/></Pstr>
                                    <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                                    <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                                </xsl:if>
                                <xsl:choose>
                                    <xsl:when test="boolean(organization[string-length(text())>0])">
                                        <Organisation>
                                            <Name>
                                                <xsl:value-of select="organization"/>
                                            </Name>
                                            <Address>  <!-- address still required on Organisation, but not on Member and not on Individual -->
                                                <xsl:call-template name="processCommonFields">
                                                    <xsl:with-param name="fieldStr" select="'addr'"/>
                                                    <xsl:with-param name="newTag" select="'Line'"/>
                                                </xsl:call-template>
                                                <xsl:if test="boolean(postcode[string-length(text())>0])">
                                                    <PostCode>
                                                        <xsl:value-of select="postcode"/>
                                                    </PostCode>
                                                </xsl:if>
                                                <Country>
                                                    <xsl:value-of select="country"/>
                                                </Country>
                                            </Address>
                                            <CRN>
                                                <xsl:value-of select="crn"/>
                                            </CRN>
                                        </Organisation>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <Individual>
                                            <Name>
                                                <xsl:if test="boolean(title[string-length(text())>0])">
                                                    <Ttl>
                                                        <xsl:value-of select="title"/>
                                                    </Ttl>
                                                </xsl:if>
                                                <xsl:if test="boolean(forename[string-length(text())>0])">
                                                    <Fore>
                                                        <xsl:value-of select="forename"/>
                                                    </Fore>
                                                </xsl:if>
                                                <xsl:if test="boolean(forename2[string-length(text())>0])">
                                                    <Fore>
                                                        <xsl:value-of select="forename2"/>
                                                    </Fore>
                                                </xsl:if>
                                                <xsl:if test="boolean(surname[string-length(text())>0])">
                                                    <Sur>
                                                        <xsl:value-of select="surname"/>
                                                    </Sur>
                                                </xsl:if>
                                            </Name>
                                            <xsl:choose>
                                                <xsl:when test="boolean(nino[string-length(text())>0])">
                                                    <NINO>
                                                        <xsl:value-of select="nino"/>
                                                    </NINO>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                                        <DOB>
                                                            <xsl:value-of select="birth_date"/>
                                                        </DOB>
                                                    </xsl:if>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </Individual>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </OnBehalfMember>
                        </xsl:when>
                        <xsl:otherwise>
                            <Member>
                                <xsl:if test="boolean(pstr[string-length(text())>0])">
                                    <Pstr><xsl:value-of select="pstr"/></Pstr>
                                    <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                                    <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                                </xsl:if>
                                <Name>
                                    <xsl:if test="boolean(title[string-length(text())>0])">
                                        <Ttl>
                                            <xsl:value-of select="title"/>
                                        </Ttl>
                                    </xsl:if>
                                    <xsl:if test="boolean(forename[string-length(text())>0])">
                                        <Fore>
                                            <xsl:value-of select="forename"/>
                                        </Fore>
                                    </xsl:if>
                                    <xsl:if test="boolean(forename2[string-length(text())>0])">
                                        <Fore>
                                            <xsl:value-of select="forename2"/>
                                        </Fore>
                                    </xsl:if>
                                    <xsl:if test="boolean(surname[string-length(text())>0])">
                                        <Sur>
                                            <xsl:value-of select="surname"/>
                                        </Sur>
                                    </xsl:if>
                                </Name>
                                <xsl:choose>
                                    <xsl:when test="boolean(nino[string-length(text())>0])">
                                        <NINO>
                                            <xsl:value-of select="nino"/>
                                        </NINO>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                            <DOB>
                                                <xsl:value-of select="birth_date"/>
                                            </DOB>
                                        </xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </Member>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:apply-templates select="payment"/>
                </Recipient>
            </xsl:when>
            <xsl:when
                    test="(event_type='PaymentsExceedingHalfSLA') and (event_type=$eventTypePassed)">
                <Member>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <xsl:choose>
                        <xsl:when test="boolean(nino[string-length(text())>0])">
                            <NINO>
                                <xsl:value-of select="nino"/>
                            </NINO>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <DOB>
                                    <xsl:value-of select="birth_date"/>
                                </DOB>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="BeneficiaryDetails"/>
                </Member>
            </xsl:when>
            <xsl:when test="(event_type='BenefitCrystallisation') and (event_type=$eventTypePassed)">
                <Member>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <xsl:choose>
                        <xsl:when test="boolean(nino[string-length(text())>0])">
                            <NINO>
                                <xsl:value-of select="nino"/>
                            </NINO>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <DOB>
                                    <xsl:value-of select="birth_date"/>
                                </DOB>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="payment"/>
                </Member>
            </xsl:when>
            <xsl:when
                    test="(event_type='AnnualAllowance') and (event_type=$eventTypePassed)">
                <Member>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <NINO>
                        <xsl:value-of select="nino"/>
                    </NINO>
                    <AggregatePensionInputAmount>
                        <xsl:value-of select="AnnualEvt22Allowance/annual_contribution_amt" />
                    </AggregatePensionInputAmount>
                    <TaxYear>
                        <xsl:value-of select="AnnualEvt22Allowance/tax_year" />
                    </TaxYear>
                    <HasMoneyPurchase>
                        <xsl:value-of select="AnnualEvt22Allowance/has_money_purchase_flag" />
                    </HasMoneyPurchase>
                    <xsl:if test="boolean(AnnualEvt22Allowance/has_money_purchase_flag[text()='yes'])">
                        <AggregateMoneyPurchaseInputAmount>
                            <xsl:value-of select="AnnualEvt22Allowance/money_purchase_input_amt" />
                        </AggregateMoneyPurchaseInputAmount>
                    </xsl:if>
                </Member>
            </xsl:when>

            <xsl:when
                    test="(event_type='LargeCommencementLumpSums') and (event_type=$eventTypePassed)">
                <Member>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <!-- no more Address in LargeCommencementLumpSums, since there is no more Address in MemberDetailsStructure in V8 schema -->
                    <xsl:choose>
                        <xsl:when test="boolean(nino[string-length(text())>0])">
                            <NINO>
                                <xsl:value-of select="nino"/>
                            </NINO>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <DOB>
                                    <xsl:value-of select="birth_date"/>
                                </DOB>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="payment"/>
                </Member>
            </xsl:when>
            <xsl:when
                    test="(event_type='EnhancedProtectionLumpSums') and (event_type = $eventTypePassed)">
                <Member>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <!-- no more Address in EnhancedProtectionLumpSums, since there is no more Address in MemberDetailsStructure in V8 schema -->
                    <xsl:choose>
                        <xsl:when test="boolean(nino[string-length(text())>0])">
                            <NINO>
                                <xsl:value-of select="nino"/>
                            </NINO>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <DOB>
                                    <xsl:value-of select="birth_date"/>
                                </DOB>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="payment"/>
                </Member>
            </xsl:when>
            <xsl:when
                    test="(event_type='FlexibleDrawdownPayments') and (event_type=$eventTypePassed)">
                <Recipient>
                    <xsl:if test="boolean(pstr[string-length(text())>0])">
                        <Pstr><xsl:value-of select="pstr"/></Pstr>
                        <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                        <xsl:if test="boolean(member_account_id[string-length(text())>0])">
                            <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                        </xsl:if>
                    </xsl:if>
                    <Name>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Name>
                    <xsl:choose>
                        <xsl:when test="boolean(nino[string-length(text())>0])">
                            <NINO>
                                <xsl:value-of select="nino"/>
                            </NINO>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <DOB>
                                    <xsl:value-of select="birth_date"/>
                                </DOB>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="boolean(country[string-length(text())>0])">
                        <Country>
                            <xsl:value-of select="country"/>
                        </Country>
                    </xsl:if>
                    <xsl:apply-templates select="payment"/>
                </Recipient>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--
        | process EmployerDetails tag : compulsory element:
        | according to whether containing Organization tag or not, list organization information
        | otherwise list Individual informatiom. list payment and all its sub elements due to it is a compulsory element
        -->
    <xsl:template match="EmployerDetails">
        <Recipient>
            <xsl:choose>
                <xsl:when test="boolean(ext_entity_flag[contains($EXTERNAL_FLAG, text())])">
                    <OnBehalfEmployer>
                        <xsl:if test="boolean(pstr[string-length(text())>0])">
                            <Pstr><xsl:value-of select="pstr"/></Pstr>
                            <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                            <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="(boolean(birth_date))">
                                <Individual>
                                    <Name>
                                        <xsl:if test="boolean(title[string-length(text())>0])">
                                            <Ttl>
                                                <xsl:value-of select="title"/>
                                            </Ttl>
                                        </xsl:if>
                                        <xsl:if test="boolean(forename[string-length(text())>0])">
                                            <Fore>
                                                <xsl:value-of select="forename"/>
                                            </Fore>
                                        </xsl:if>
                                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                                            <Fore>
                                                <xsl:value-of select="forename2"/>
                                            </Fore>
                                        </xsl:if>
                                        <xsl:if test="boolean(surname[string-length(text())>0])">
                                            <Sur>
                                                <xsl:value-of select="surname"/>
                                            </Sur>
                                        </xsl:if>
                                    </Name>
                                    <xsl:choose>
                                        <xsl:when test="boolean(nino[string-length(text())>0])">
                                            <NINO>
                                                <xsl:value-of select="nino"/>
                                            </NINO>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                                <DOB>
                                                    <xsl:value-of select="birth_date"/>
                                                </DOB>
                                            </xsl:if>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </Individual>
                            </xsl:when>
                            <xsl:otherwise>
                                <Organisation>
                                    <Name>
                                        <xsl:value-of select="organization"/>
                                    </Name>
                                    <Address>
                                        <xsl:call-template name="processCommonFields">
                                            <xsl:with-param name="fieldStr" select="'addr'"/>
                                            <xsl:with-param name="newTag" select="'Line'"/>
                                        </xsl:call-template>
                                        <xsl:if test="boolean(postcode[string-length(text())>0])">
                                            <PostCode>
                                                <xsl:value-of select="postcode"/>
                                            </PostCode>
                                        </xsl:if>
                                        <Country>
                                            <xsl:value-of select="country"/>
                                        </Country>
                                    </Address>
                                    <CRN>
                                        <xsl:value-of select="crn"/>
                                    </CRN>
                                </Organisation>
                            </xsl:otherwise>
                        </xsl:choose>
                    </OnBehalfEmployer>
                </xsl:when>
                <xsl:otherwise>
                    <Employer>
                        <xsl:if test="boolean(pstr[string-length(text())>0])">
                            <Pstr><xsl:value-of select="pstr"/></Pstr>
                            <EventType><xsl:value-of select="event_type"></xsl:value-of></EventType>
                            <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="not(boolean(birth_date))">
                                <Individual>
                                    <Name>
                                        <xsl:if test="boolean(title[string-length(text())>0])">
                                            <Ttl>
                                                <xsl:value-of select="title"/>
                                            </Ttl>
                                        </xsl:if>
                                        <xsl:if test="boolean(forename[string-length(text())>0])">
                                            <Fore>
                                                <xsl:value-of select="forename"/>
                                            </Fore>
                                        </xsl:if>
                                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                                            <Fore>
                                                <xsl:value-of select="forename2"/>
                                            </Fore>
                                        </xsl:if>
                                        <xsl:if test="boolean(surname[string-length(text())>0])">
                                            <Sur>
                                                <xsl:value-of select="surname"/>
                                            </Sur>
                                        </xsl:if>
                                    </Name>
                                    <xsl:choose>
                                        <xsl:when test="boolean(nino[string-length(text())>0])">
                                            <NINO>
                                                <xsl:value-of select="nino"/>
                                            </NINO>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                                <DOB>
                                                    <xsl:value-of select="birth_date"/>
                                                </DOB>
                                            </xsl:if>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </Individual>
                            </xsl:when>
                            <xsl:otherwise>
                                <Organisation>
                                    <Name>
                                        <xsl:value-of select="organization"/>
                                    </Name>
                                    <Address>
                                        <xsl:call-template name="processCommonFields">
                                            <xsl:with-param name="fieldStr" select="'addr'"/>
                                            <xsl:with-param name="newTag" select="'Line'"/>
                                        </xsl:call-template>
                                        <xsl:if test="boolean(postcode[string-length(text())>0])">
                                            <PostCode>
                                                <xsl:value-of select="postcode"/>
                                            </PostCode>
                                        </xsl:if>
                                        <Country>
                                            <xsl:value-of select="country"/>
                                        </Country>
                                    </Address>
                                    <CRN>
                                        <xsl:value-of select="crn"/>
                                    </CRN>
                                </Organisation>
                            </xsl:otherwise>
                        </xsl:choose>
                    </Employer>
                    <xsl:apply-templates select="payment"/>
                </xsl:otherwise>
            </xsl:choose>
        </Recipient>
    </xsl:template>


    <!-- ********************************************************************** -->
    <!-- *******************    prcess payment part            **************** -->
    <!-- ********************************************************************** -->

    <!--
        | process payment tag with event_type='UnauthorisedPayments': compulsory element
        -->
    <xsl:template
            match="MemberDetails[event_type='UnauthorisedPayments']/payment">
        <Payment>
            <xsl:choose>
                <xsl:when test="boolean(payment_name[contains($NATURE_PATTERN, text())])">
                    <Nature>
                        <xsl:value-of select="payment_name"/>
                    </Nature>
                </xsl:when>
                <xsl:otherwise>
                    <Other>
                        <xsl:value-of select="payment_name"/>
                    </Other>
                </xsl:otherwise>
            </xsl:choose>
            <Amount>
                <xsl:value-of select="amount_paid"/>
            </Amount>
            <Date>
                <xsl:value-of select="date_paid"/>
            </Date>
        </Payment>
    </xsl:template>

    <!--
        | process payment tag with event_type='BenefitCrystallisation': compulsory element
        -->
    <xsl:template
            match="MemberDetails[event_type='BenefitCrystallisation']/payment">
        <Event>
            <Amount>
                <xsl:value-of select="amount_paid"/>
            </Amount>
            <Date>
                <!--Modified as part of  Tracker Issue 2903 to include the date as of crystallisation- Starts Here-->
                <xsl:value-of select="date_cry"/>
                <!--Modified as part of  Tracker Issue 2903 to include the date as of crystallisation- Ends Here-->
            </Date>
            <Reference>
                <xsl:value-of select="hmrc_refno"/>
            </Reference>
        </Event>
    </xsl:template>

    <!--
        | process payment tag with event_type='LargeCommencementLumpSums': compulsory element
        -->
    <xsl:template
            match="MemberDetails[event_type='LargeCommencementLumpSums']/payment">
        <Payment>
            <AmountCrystallised>
                <xsl:value-of select="amount_cry"/>
            </AmountCrystallised>
            <AmountLumpSum>
                <xsl:value-of select="amount_lumpsum"/>
            </AmountLumpSum>
            <Date>
                <xsl:value-of select="date_cry"/>
            </Date>
        </Payment>
    </xsl:template>

    <!--
        | process payment tag with event_type='EnhancedProtectionLumpSums': compulsory element
        -->
    <xsl:template
            match="MemberDetails[event_type='EnhancedProtectionLumpSums']/payment">
        <Payment>
            <Amount>
                <xsl:value-of select="amount_paid"/>
            </Amount>
            <Date>
                <xsl:value-of select="date_cry"/>
            </Date>
            <Reference>
                <xsl:value-of select="hmrc_refno"/>
            </Reference>
        </Payment>
    </xsl:template>

    <!--
    | process payment tag with event_type='FlexibleDrawdownPayments': compulsory element
    -->
    <xsl:template
            match="MemberDetails[event_type='FlexibleDrawdownPayments']/payment">
        <TotalPaymentsAmount>
            <xsl:value-of select="amount_paid"/>
        </TotalPaymentsAmount>
    </xsl:template>

    <!--
        | process payment tag with event_type='PaymentsExceedingHalfSLA': compulsory element
    -->
    <xsl:template
            match="MemberDetails[event_type='PaymentsExceedingHalfSLA']/BeneficiaryDetails/payment">
        <Payment>
            <Amount>
                <xsl:value-of select="amount_paid"/>
            </Amount>
            <Date>
                <xsl:value-of select="payment_date"/>
            </Date>
        </Payment>
    </xsl:template>
    <!--Added as part of  Tracker Issue 2906,2907,2908 to fix the date and payment amount problem - Ends Here-->

    <!--Added as part of  Tracker Issue 2456 to fix the multiple transaction issue for Employer Details for Unauthorised Payments - Ends Here-->
    <xsl:template
            match="EmployerDetails[event_type='UnauthorisedPayments']/payment">
        <Payment>
            <xsl:choose>
                <xsl:when test="boolean(payment_name[contains($NATURE_PATTERN, text())])">
                    <Nature>
                        <xsl:value-of select="payment_name"/>
                    </Nature>
                </xsl:when>
                <xsl:otherwise>
                    <Other>
                        <xsl:value-of select="payment_name"/>
                    </Other>
                </xsl:otherwise>
            </xsl:choose>
            <Amount>
                <xsl:value-of select="amount_paid"/>
            </Amount>
            <Date>
                <xsl:value-of select="date_paid"/>
            </Date>
        </Payment>
    </xsl:template>
    <!--Added as part of  Tracker Issue 2456 to fix the multiple transaction issue for Employer Details for Unauthorised Payments - Ends Here-->

    <!-- ********************************************************************** -->
    <!-- *******************    process Recepient part          **************** -->
    <!-- ********************************************************************** -->

    <!--
        | process BeneficiaryDetails tag with event_type='PaymentsExceedingHalfSLA'
        -->
    <xsl:template
            match="MemberDetails[event_type='PaymentsExceedingHalfSLA']/BeneficiaryDetails">
        <Recipient>
            <xsl:choose>
                <!--Added as part of  Tracker Issue 3312 to fix the Organization  problem - Starts Here-->
                <xsl:when test="boolean(entity_type[contains($RECIPIENT_TYPE, text())])">
                    <!--Added as part of  Tracker Issue 3312 to fix the Organization  problem - Ends Here-->
                    <!--xsl:when test="not(boolean(organization))"-->
                    <Individual>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Individual>
                </xsl:when>
                <xsl:otherwise>
                    <Organisation>
                        <!--xsl:value-of select="organization"/-->
                        <!--Added as part of  Tracker Issue 3312 to fix the Organization  problem - Starts Here-->
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <xsl:value-of select="surname"/>
                        </xsl:if>
                        <!--Added as part of  Tracker Issue 3312 to fix the Organization  problem - Ends Here-->
                    </Organisation>
                </xsl:otherwise>
            </xsl:choose>
            <Address> <!-- address always required for PaymentsExceedingHalfSLA/Member/Recipient  regardless of entity_type -->
                <xsl:call-template name="processCommonFields">
                    <xsl:with-param name="fieldStr" select="'addr'"/>
                    <xsl:with-param name="newTag" select="'Line'"/>
                </xsl:call-template>
                <xsl:if test="boolean(postcode[string-length(text())>0])">
                    <PostCode>
                        <xsl:value-of select="postcode"/>
                    </PostCode>
                </xsl:if>
                <Country>
                    <xsl:value-of select="country"/>
                </Country>
            </Address>
            <!--Added as part of  Tracker Issue 2906,2907,2908 to fix the date and payment amount problem - Starts Here-->
            <xsl:apply-templates select="payment"/>
            <!--Added as part of  Tracker Issue 2906,2907,2908 to fix the date and payment amount problem - Ends Here-->
        </Recipient>
    </xsl:template>

    <!--
    | process Declaration tag
    -->
    <xsl:template match="Declaration">
        <Declaration>
            <Administrator>
                <xsl:choose>
                    <xsl:when test="not(organization)">
                        <Individual>
                            <xsl:if test="boolean(title[string-length(text())>0])">
                                <Ttl>
                                    <xsl:value-of select="title"/>
                                </Ttl>
                            </xsl:if>
                            <xsl:if test="boolean(forename[string-length(text())>0])">
                                <Fore>
                                    <xsl:value-of select="forename"/>
                                </Fore>
                            </xsl:if>
                            <xsl:if test="boolean(forename2[string-length(text())>0])">
                                <Fore>
                                    <xsl:value-of select="forename2"/>
                                </Fore>
                            </xsl:if>
                            <xsl:if test="boolean(surname[string-length(text())>0])">
                                <Sur>
                                    <xsl:value-of select="surname"/>
                                </Sur>
                            </xsl:if>
                        </Individual>
                    </xsl:when>
                    <xsl:otherwise>
                        <Organisation>
                            <xsl:value-of select="organization"/>
                        </Organisation>
                    </xsl:otherwise>
                </xsl:choose>
                <Address>
                    <xsl:call-template name="processCommonFields">
                        <xsl:with-param name="fieldStr" select="'addr'"/>
                        <xsl:with-param name="newTag" select="'Line'"/>
                    </xsl:call-template>
                    <xsl:if test="boolean(postcode[string-length(text())>0])">
                        <PostCode>
                            <xsl:value-of select="postcode"/>
                        </PostCode>
                    </xsl:if>
                    <Country>
                        <xsl:value-of select="country"/>
                    </Country>
                </Address>
                <xsl:if test="boolean(phone_number[string-length(text())>0])">
                    <Telephone>
                        <xsl:value-of select="phone_number"/>
                    </Telephone>
                </xsl:if>
                <xsl:if test="boolean(email_address[string-length(text())>0])">
                    <Email>
                        <xsl:value-of select="child::email_address"/>
                    </Email>
                </xsl:if>
            </Administrator>
            <xsl:choose>
                <xsl:when
                        test="(count(InformationCorrect)>0)
                                and
                                (count(NoFalseStatements)>0)">
                    <xsl:copy-of select="InformationCorrect"/>
                    <xsl:copy-of select="NoFalseStatements"/>
                </xsl:when>
                <xsl:when
                        test="(count(ApprovedByAdministrator)>0)
                                and
                                (count(Authorized)>0)">
                    <xsl:copy-of select="ApprovedByAdministrator"/>
                    <xsl:copy-of select="Authorized"/>
                </xsl:when>
            </xsl:choose>
        </Declaration>
    </xsl:template>

    <!--
        | process some common tags which start with the same string (eg, address1, address2, ..);
        | input parameters:
        |     fieldStr: string which is the common part of source tag name shared by all tags (eg. "address")
        |     newTag: new targt tag name
        -->
    <xsl:template name="processCommonFields">
        <xsl:param name="fieldStr"/>
        <xsl:param name="newTag"/>
        <xsl:for-each select="*[starts-with(name(),$fieldStr)]">
            <xsl:sort select="name()"/>
            <xsl:if test="boolean(current()[string-length(text())>0])">
                <xsl:element name="{$newTag}">
                    <xsl:choose>
                        <xsl:when test="text()='unknown'">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
