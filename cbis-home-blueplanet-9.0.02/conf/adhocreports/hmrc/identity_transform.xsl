<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"    
    xmlns="http://www.govtalk.gov.uk/CM/envelope" >
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="hmrc">
        <xsl:copy-of select="."/>  <!--  IDEM transformer just write the XML to the output --> 		
	</xsl:template>
</xsl:stylesheet>