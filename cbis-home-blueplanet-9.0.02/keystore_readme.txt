The keystore for testing purpose can be created using the following command.
The following command will create a keystore with cert that expires in 10 years.

keytool -genkey -alias selfsigned -keyalg RSA -keystore keystore.jks -validity 3653 -keysize 2048

Using this command will follow the below prompt to set up the keystore file.


Enter keystore password:
Re-enter new password:
What is your first and last name?
  [Unknown]:  Test
What is the name of your organizational unit?
  [Unknown]:  WM
What is the name of your organization?
  [Unknown]:  GBST
What is the name of your City or Locality?
  [Unknown]:  Sydney
What is the name of your State or Province?
  [Unknown]:  NSW
What is the two-letter country code for this unit?
  [Unknown]:  AU
Is CN=Test, OU=WM, O=GBST, L=Sydney, ST=NSW, C=AU correct?
  [no]:  Y

Enter key password for <mykey>
        (RETURN if same as keystore password):


NOTE: the keystore path can be configured in the cbis properties